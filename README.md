# Flight Reservation

## Step1: Create Database and add tables
Below are the sql statements to use

```
CREATE DATABASE RESERVATION;

USE RESERVATION;

# Create table user
CREATE TABLE USER 
(
ID INT NOT NULL AUTO_INCREMENT,
FIRST_NAME VARCHAR(20),
LAST_NAME VARCHAR(20),
EMAIL VARCHAR(20),
PASSWORD VARCHAR(256), 
PRIMARY KEY (ID),
UNIQUE KEY (EMAIL)
);

# Create table flight
CREATE TABLE FLIGHT
(
  ID INT  NOT NULL AUTO_INCREMENT,
  FLIGHT_NUMBER VARCHAR(20)  NOT NULL, 
  OPERATING_AIRLINES VARCHAR(20)  NOT NULL,
  DEPARTURE_CITY VARCHAR(20)  NOT NULL,
  ARRIVAL_CITY VARCHAR(20)  NOT NULL,
  DATE_OF_DEPARTURE DATE  NOT NULL,
  ESTIMATED_DEPARTURE_TIME TIMESTAMP DEFAULT CURRENT_TIMESTAMP,  
  PRIMARY KEY (ID)
);

# Create table passenger
CREATE TABLE PASSENGER
(
  ID         INT NOT NULL AUTO_INCREMENT,
  FIRST_NAME       VARCHAR(256),
  LAST_NAME    VARCHAR(256),
  MIDDLE_NAME   VARCHAR(256),
  EMAIL VARCHAR(50),
  PHONE VARCHAR(10),
  PRIMARY KEY (ID)
);

# Create table reservation
CREATE TABLE RESERVATION
(
  ID INT NOT NULL AUTO_INCREMENT,
  CHECKED_IN TINYINT(1),
  NUMBER_OF_BAGS INT,
  PASSENGER_ID INT,
  FLIGHT_ID INT,
  CREATED TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (ID),
  FOREIGN KEY (PASSENGER_ID) REFERENCES PASSENGER(ID) ON DELETE CASCADE,
  FOREIGN KEY (FLIGHT_ID) REFERENCES FLIGHT(ID)
);

SELECT * FROM USER;

SELECT * FROM PASSENGER;

SELECT * FROM FLIGHT;

SELECT * FROM RESERVATION;

# Use drop command in case something goes wrong and you want to change it
DROP TABLE USER;
DROP TABLE RESERVATION;
DROP TABLE PASSENGER;
DROP TABLE FLIGHT;

DROP DATABASE RESERVATION;

```

## Step2: Download 
Help --> Eclipse Marketplace --> Search Eclipse Enterprise and java web developer tools

https://gitlab.com/manish.garg2/flight-reservation-system/-/blob/3a7a2a330dab4f06eb3d6893f0bb6c30a9b7c53e/flightreservation/images/image.png

## Step3: Create spring boot project 
Click on file --> New --> Spring Starter Project and fill below details
```
Group: com.bca.flightreservation
Aritfact: flightreservation
Description: Flight Reservation Module
Package: com.bca.flightreservation
```
Click Next
```
Add My SQL driver, Spring data JPA, Spring Web from the search option
```
Click finish

## Step4: Create Model Classes for all the tables
```
Click on package
Select new Class
Name: User
Package: com.bca.reservation.entities
```

Sample Model class for User is there in the below path 
```
flightreservation/src/main/java/com/bca/flightreservation/entities
```
Create Model classes for Passenger Flight Reservation

## Step5: Create Repository Interface
```
Click on package
Select new Interface
Name: UserRepository
Package: com.bca.reservation.repos
```

Sample Repo class for UserRepository is there in the below path
```
flightreservation/src/main/java/com/bca/flightreservation/repos
```

## Step6: Create Controller class as a part of Presentation Layer
```
Click on package
Select new class
Name: UserController
Package: com.bca.reservation.controller
```

Sample Controller class for UserRepository in there in the below path
```
flightreservation/src/main/java/com/bca/flightreservation/controller
```

## Step7: Fix application.properties inside resources 
Enter your password and username
```
# Connect to DB
spring.datasource.url=jdbc:mysql://localhost:3306/reservation
spring.datasource.username=root
spring.datasource.password=

spring.jpa.show-sql=true

# spring.mvc.view.prefix=/WEB-INF/jsps/
# spring.mvc.view.suffix=.jsp

# Context path
server.servlet.context-path=/flightreservation

# spring.thymeleaf.cache=false

```

## Step8: Fix pom.xml


# Assignment 2

## User Registration Feature
User should be able to register. 
Flow should be like :
User registration html page --> Data saved to DB --> Redirect to login page

## User login Feature
Flow:
User login page --> If the details are correct --> Redirect to Flight search
                --> If the details are incorrect --> Redirect to same login page and show error message

## Search Flights Feature
Load flight data to Flight table in db
Flow:
Flight search page - User should be able to put source, destination and travel date and click on submit
Details of all the flights should be available after the submit

## Handle flight selection
There should be a select button at the end of every flight, 
If the user selects the flight, it should redirect to the confirm reservation page.

## Create Reservation Feature
Create reservation include taking the payment info from user and passengar details.
We can take dummy payment information.
We should create a service layer here between controller and repository, because there are different business login of payment and reservation involed. Everything should not be done in controller.

